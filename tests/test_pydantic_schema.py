from datetime import datetime

from pydantic import BaseModel
from similar_dict import similar_dict


class Author(BaseModel):
    given_name: str
    surname: str


class Book(BaseModel):
    title: str
    published_at: datetime
    author: Author


expected_literal_schema = {
    "$defs": {
        "Author": {
            "properties": {
                "given_name": {"title": "Given Name", "type": "string"},
                "surname": {"title": "Surname", "type": "string"},
            },
            "required": ["given_name", "surname"],
            "title": "Author",
            "type": "object",
        }
    },
    "properties": {
        "title": {"title": "Title", "type": "string"},
        "published_at": {
            "format": "date-time",
            "title": "Published At",
            "type": "string",
        },
        "author": {"$ref": "#/$defs/Author"},
    },
    "required": ["title", "published_at", "author"],
    "title": "Book",
    "type": "object",
}

expected_similar_schema = {
    "$defs": {
        "Author": {
            "properties": {
                "given_name": {"title": str, "type": "string"},
                "surname": {"title": str, "type": "string"},
            },
            "required": ["given_name", "surname"],
            "title": str,
            "type": "object",
        }
    },
    "properties": {
        "title": {"title": str, "type": "string"},
        "published_at": {
            "format": "date-time",
            "title": str,
            "type": "string",
        },
        "author": {"$ref": "#/$defs/Author"},
    },
    "required": ["title", "published_at", "author"],
    "title": str,
    "type": "object",
}


def test_literal():
    actual = Book.model_json_schema()
    assert actual == expected_literal_schema, "Literal does not match"
    assert similar_dict(actual, expected_literal_schema), "Literal does not match"


def test_similar():
    actual = Book.model_json_schema()
    assert actual != expected_similar_schema, "Similar matches exact"
    assert similar_dict(actual, expected_similar_schema), "Similar matches"
